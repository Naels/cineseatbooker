const express = require('express');
const movieRouter     = require('./routes/movieRoutes');
const seatRouter      = require('./routes/seatRoutes');
const showtimeRouter  = require('./routes/showtimeRoutes');
const userRouter      = require('./routes/userRoutes');

const swaggerUi = require('swagger-ui-express')
swaggerDocument = require('./swagger.json')

const cors = require('cors')

const app = express();
app.use(cors())
app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument))


const port = 3000;

app.use(express.json());
app.use(movieRouter);
app.use(seatRouter)
app.use(showtimeRouter)


const server = app.listen(port, () => {
  console.log(`Server is running on port ${server.address().port}`);
});