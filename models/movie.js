const connection = require('../config/config');

class Movie {
  static getAll(callback) {
    const query = 'SELECT * FROM movies';
    connection.query(query, (err, results) => {
      if (err) {
        console.error('Erreur lors de la récupération des films :', err);
        callback(err, null);
        return;
      }
      callback(null, results);
    });
  }

  static getById(movieId, callback) {
    const query = 'SELECT * FROM movies WHERE movie_id = ?';
    connection.query(query, [movieId], (err, results) => {
      if (err) {
        console.error('Erreur lors de la récupération du film :', err);
        callback(err, null);
        return;
      }
      if (results.length === 0) {
        callback(null, null); 
        return;
      }
      callback(null, results[0]);
    });
  }

  static create(newMovie, callback) {
    const query = 'INSERT INTO movies SET ?';
    connection.query(query, newMovie, (err, result) => {
      if (err) {
        console.error('Erreur lors de la création du film :', err);
        callback(err, null);
        return;
      }
      const createdMovie = { id: result.insertId, ...newMovie };
      callback(null, createdMovie);
    });
  }

  static update(movieId, updatedMovie, callback) {
    const query = 'UPDATE movies SET ? WHERE id = ?';
    connection.query(query, [updatedMovie, movieId], (err, result) => {
      if (err) {
        console.error('Erreur lors de la mise à jour du film :', err);
        callback(err, null);
        return;
      }
      if (result.affectedRows === 0) {
        callback(null, null); 
        return;
      }
      callback(null, updatedMovie);
    });
  }

  static delete(movieId, callback) {
    const query = 'DELETE FROM movies WHERE id = ?';
    connection.query(query, [movieId], (err, result) => {
      if (err) {
        console.error('Erreur lors de la suppression du film :', err);
        callback(err, null);
        return;
      }
      if (result.affectedRows === 0) {
        callback(null, null); 
        return;
      }
      callback(null, true);
    });
  }

  static getShowtimesByMovieId(movieId, callback) {
    const query = 'SELECT * FROM showtimes WHERE movie_id = ?';
    connection.query(query, [movieId], (err, results) => {
      if (err) {
        console.error('Erreur lors de la récupération des créneaux horaires :', err);
        callback(err, null);
        return;
      }
      callback(null, results);
    });
  }



}

module.exports = Movie;