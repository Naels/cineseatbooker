const connection = require('../config/config');

class Showtime {

  static getAll(callback) {
    const query = 'SELECT * FROM showtimes';
    connection.query(query, (err, results) => {
      if (err) {
        console.error('Error retrieving seats:', err);
        callback(err, null);
        return;
      }
      callback(null, results);
    });
  }

  static getShowtimesByMovieId(movie_id, callback) {
    const query = 'SELECT * FROM showtimes WHERE movie_id = ?';
    connection.query(query, [movie_id], (err, results) => {
      if (err) {
        console.error('Error retrieving seats:', err);
        callback(err, null);
        return;
      }
      callback(null, results);
    });
  }


  static getShowtimeById(showtime_id, callback) {
    const query = `SELECT * FROM showtimes WHERE showtime_id = ?`
    connection.query(query, [showtime_id], (err, res) =>{
      if(err) {
        console.log('error retrieving showtime infos:', err)
        callback(err, null)
        return
      }
      callback(null, res)
    })
  }

}



module.exports = Showtime;