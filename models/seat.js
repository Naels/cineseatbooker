const connection = require('../config/config');

class Seat {

  static getSeats(showtimeId, callback) {
    const query = 'SELECT * FROM seats WHERE showtime_id = ?';
    connection.query(query, [showtimeId], (err, results) => {
      if (err) {
        console.error('Error retrieving seats from the database:', err);
        callback(err, null);
        return;
      }

      const seats = results;

      callback(null, seats);
    });
  }  
  
  static countAvailableSeatsByShowtime(showtimeId, callback) {
    const query = 'SELECT count(*) FROM seats WHERE showtime_id = ? AND is_taken = 0';
    connection.query(query, [showtimeId], (err, results) => {
      if (err) {
        console.error('Error retrieving available seats:', err);
        callback(err, null);
        return;
      }
      callback(null, results[0]);
    });
  }

  static reserveSeats(showtimeId, seatsToReserve, callback) {
    this.getSeats(showtimeId, (err, seats) => {
      if (err) {
        console.error('Error retrieving seats:', err);
        return callback(err);
      }

      const reservedSeats = seats.filter((seat) =>
        seatsToReserve.includes(seat.seat_id)
      );

      const isAllSeatsAvailable = reservedSeats.every((seat) => seat.is_taken === 0);

      if (!isAllSeatsAvailable) {
        const unavailableSeats = reservedSeats.filter((seat) => seat.is_taken);
        const errorMessage = `Seats ${unavailableSeats.map((seat) => seat.seat_id).join(', ')} are already reserved.`;
        return callback(new Error(errorMessage));
      }

      reservedSeats.forEach((seat) => {
        this.updateReservationStatus(seat.seat_id, showtimeId, (err) => {
          if (err) {
            console.error('Error updating seat reservation status:', err);
            return callback(err);
          }
        });
      });

      callback(null);
    });
  }

  static updateReservationStatus(seatId, showtimeId, callback) {
    const query = `
      UPDATE seats
      SET is_taken = TRUE
      WHERE seat_id = ? AND showtime_id = ?
    `;
  
    const params = [seatId, showtimeId];

    connection.query(query, params, (err, result) => {
      if (err) {
        console.error('Error updating seat reservation status:', err);
        return callback(err);
      }
      callback(null); 
    });
  }
}

module.exports = Seat;