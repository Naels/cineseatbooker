# CinSeatBooker Service

This Node.js service provides an API for booking precise seats in a cinema room for a specific film and showtime. It is connected to a MySQL database to store and manage seat availability and bookings.


### Prerequisites

Before running the application, make sure you have the following installed (at least):

    Node.js (version 14)
    MySQL (version 3.4.0)

## Installation

1. Clone the repository 
2. Navigate to the project directory
3. Install the dependencies using `npm i`
4. Set up the MySQL database using `Dump-seatbooker`
5. Configure the database connection inside the `config.js` file
6. Start the service using `npm start`


### Usage

The API exposes the following endpoints (few examples):

GET:  /movies - Get a list of movies.
GET:  /films/:id - Get informations about one specific movie.
GET:  /showtimes/:movieId - Get all showtimes for a specific movie.
POST: /seats/:showtimeId/seats - Book seats for a specific movie and showtime.

Refer to the API documentation for detailed information on request and response formats.

Happy cinema seat booking!
