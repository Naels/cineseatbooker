const express = require('express');
const Showtime = require('../models/showtime');
const Seat = require('../models/seat')

const router = express.Router();

// GET /showtimes
router.get('/showtimes', (req,res) => {
  Showtime.getAll((err, showtimes) => {
    if (err) {
      console.error('Error retrieving showtimes:', err);
      res.status(500).json({ error: 'An error occurred while retrieving showtimes' });
      return;
    }
    res.json(showtimes);
  });
});


// GET /showtimes/:movieId/
router.get('/showtimes/:movieId', (req, res) => {
  const movieId = req.params.movieId;
  Showtime.getShowtimesByMovieId(movieId, (err, showtimes) => {
    if (err) {
      console.error('Error retrieving showtimes:', err);
      res.status(500).json({ error: 'An error occurred while retrieving showtimes' });
      return;
    }
    res.json(showtimes);
  });
});


//GET /showtimes/show/:showtimeId
router.get('/showtimes/show/:showtimeId', (req, res) => {
  const showtimeId = req.params.showtimeId;
  Showtime.getShowtimeById(showtimeId, (err, showtime) => {
    if (err) {
      console.error('Error retrieving showtime:', err);
      res.status(500).json({ error: 'An error occurred while retrieving showtime' });
      return;
    }
    res.json(showtime);
  });
})



module.exports = router;