const express = require('express');
const Movie = require('../models/movie');
const Showtime = require('../models/showtime')
const Seat = require('../models/seat')

const router = express.Router();

// GET /movies
router.get('/movies', (req, res) => {
    Movie.getAll((err, movies) => {
        if (err) {
            console.error('Error retrieving movies:', err);
            res.status(500).json({ error: 'An error occurred while retrieving movies' });
            return;
        }
        res.json(movies);
    });
});

// GET /movies/:id
router.get('/movies/:id', (req, res) => {
    const movieId = req.params.id;
    Movie.getById(movieId, (err, movie) => {
        if (err) {
            console.error('Error retrieving movie:', err);
            res.status(500).json({ error: 'An error occurred while retrieving the movie' });
            return;
        }
        if (!movie) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }
        res.json(movie);
    });
});

// POST /movies
router.post('/movies', (req, res) => {
    const newMovie = req.body;
    Movie.create(newMovie, (err, createdMovie) => {
        if (err) {
            console.error('Error creating movie:', err);
            res.status(500).json({ error: 'An error occurred while creating the movie' });
            return;
        }
        res.status(201).json(createdMovie);
    });
});

// PUT /movies/:id
router.put('/movies/:id', (req, res) => {
    const movieId = req.params.id;
    const updatedMovie = req.body;
    Movie.update(movieId, updatedMovie, (err, movie) => {
        if (err) {
            console.error('Error updating movie:', err);
            res.status(500).json({ error: 'An error occurred while updating the movie' });
            return;
        }
        if (!movie) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }
        res.json(movie);
    });
});

// DELETE /movies/:id
router.delete('/movies/:id', (req, res) => {
    const movieId = req.params.id;
    Movie.delete(movieId, (err, result) => {
        if (err) {
            console.error('Error deleting movie:', err);
            res.status(500).json({ error: 'An error occurred while deleting the movie' });
            return;
        }
        if (!result) {
            res.status(404).json({ error: 'Movie not found' });
            return;
        }
        res.json({ success: true });
    });
});



// GET /movies/:id/showtimes
router.get('/movies/:movieId/showtimes', (req, res) => {
    const { movieId } = req.params;

    Movie.getShowtimesByMovieId(movieId, (err, showtimes) => {
        if (err) {
            console.error('Erreur lors de la récupération des créneaux horaires :', err);
            res.status(500).json({ error: 'Erreur lors de la récupération des créneaux horaires' });
            return;
        }
        res.json({ showtimes });
    });
});

// GET /movies/:movieId/showtimes/:showtimeId/seats
router.get('/movies/:movieId/showtimes/:showtimeId/seats', (req, res) => {
    const { movieId, showtimeId } = req.params;

    Seat.getSeats(showtimeId, (err, seats) => {
        if (err) {
            console.error('Error retrieving seats:', err);
            res.status(500).json({ error: 'Error retrieving seats' });
            return;
        }

        res.json({ seats });
    });
});


module.exports = router;