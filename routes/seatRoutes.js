const express = require('express');
const router = express.Router();
const Seat = require('../models/seat');
const Showtime = require('../models/showtime')



//GET /seats/:showtime_id
router.get('/seats/:id', (req, res) => {
  const showtimeId = req.params.id;

  Seat.getSeats(showtimeId, (err, seats) => {
    if (err) {
      console.error('Error counting available seats:', err);
      res.status(500).json({ error: 'An error occurred while counting the available seats' });
      return;
    }
    res.json({ seats });
  });
});


// GET /seats/:id/countAvailable
// return count of available seats by showtime
router.get('/seats/:id/countAvailable', (req, res) => {
  const showtimeId = req.params.id;

  Seat.countAvailableSeatsByShowtime(showtimeId, (err, count) => {
    if (err) {
      console.error('Error counting available seats:', err);
      res.status(500).json({ error: 'An error occurred while counting the available seats' });
      return;
    }
    res.json({ availableSeats: count['count(*)'] });
  });
});


router.post('/seats/:showtimeId/seats', (req, res) => {
  const showtimeId = req.params.showtimeId;
  const seatsToReserve = req.body.seats;

  // Retrieve the seats from the database based on the showtimeId
  Seat.getSeats(showtimeId, (err, seats) => {
    if (err) {
      console.error('Error retrieving seats:', err);
      return res.status(500).json({ error: 'An error occurred while retrieving the seats' });
    }

    const availableSeats = seats.filter(seat => !seat.is_taken);
    const reservedSeats = seats.filter(seat => seat.is_taken);

    const invalidSeats = seatsToReserve.filter((seat) => {
      return reservedSeats.some(reservedSeat => reservedSeat.seat_id === seat) || !availableSeats.some(availableSeat => availableSeat.seat_id === seat);
    });

    if (invalidSeats.length > 0) {
      return res.status(400).json({ error: 'Invalid seats: ' + invalidSeats.join(', ') });
    }

    // Reserve seats
    Seat.reserveSeats(showtimeId, seatsToReserve, (err) => {
      if (err) {
        console.error('Error reserving seats:', err);
        return res.status(500).json({ error: 'An error occurred while reserving the seats' });
      }

      res.json({ success: true });
    });
  });
});

module.exports = router;